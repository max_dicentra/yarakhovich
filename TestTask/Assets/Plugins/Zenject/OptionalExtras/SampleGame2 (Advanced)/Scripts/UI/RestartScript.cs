﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject.SpaceFighter;

public class RestartScript : MonoBehaviour
{
    private bool _isDelaying;
    private float _delayStartTime;

    [SerializeField] float restartDelay;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_isDelaying)
        {
            if (Time.realtimeSinceStartup - _delayStartTime > restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    void OnPlayerDied()
    {
        _delayStartTime = Time.realtimeSinceStartup;
        _isDelaying = true;
    }

    public void OnClick()
    {
        Instances.PauseController.ContinueGame();
        Instances.DefeatWindow.gameObject.SetActive(false);
        Instances.HealthWatcher.Die();
        OnPlayerDied();
    }
}
