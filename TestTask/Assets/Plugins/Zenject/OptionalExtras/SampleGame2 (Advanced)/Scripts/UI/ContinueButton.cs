﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueButton : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Instances.ContinueButton = this;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnClick()
    {
        Instances.Player.RestoreHealth();
        Instances.DefeatWindow.gameObject.SetActive(false);
        Instances.StatsPanel.gameObject.SetActive(true);
        Instances.PauseController.ContinueGame();
    }
}
