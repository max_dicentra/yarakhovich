﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefeatWindowScript : MonoBehaviour
{
    void Awake()
    {
        Instances.DefeatWindow = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
