﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject.SpaceFighter;

public class Instances
{
    private static ContinueButton continueButton;
    private static DefeatWindowScript defeatWindow;
    private static StatsPanel statsPanel;
    private static Player player;
    private static PlayerHealthWatcher playerHealthWatcher;
    private static PauseController pauseController;
    private static EnemyRegistry enemyRegistry;

    public static ContinueButton ContinueButton
    {
        get; set;
    }

    public static DefeatWindowScript DefeatWindow
    {
        get; set;
    }

    public static Player Player
    {
        get; set;
    }

    public static PlayerHealthWatcher HealthWatcher
    {
        get; set;
    }

    public static PauseController PauseController
    {
        get;set;
    }

    public static EnemyRegistry EnemyRegistry
    {
        get; set;
    }

    public static StatsPanel StatsPanel
    {
        get;set;
    }
}
