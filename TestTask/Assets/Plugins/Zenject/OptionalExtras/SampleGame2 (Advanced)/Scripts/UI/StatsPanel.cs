﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Zenject.SpaceFighter;

public class StatsPanel : MonoBehaviour
{
    [SerializeField] private Text killCount = default;
    [SerializeField] private Slider healthSlider = default;

    int _killCount;

    [Inject]
    public void Construct(SignalBus signalBus)
    {
        signalBus.Subscribe<EnemyKilledSignal>(OnEnemyKilled);
    }

    // Start is called before the first frame update
    void Start()
    {
        healthSlider.value = healthSlider.maxValue;
        Instances.StatsPanel = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnemyKilled()
    {
        _killCount++;
        killCount.text ="Kill count: " +  _killCount.ToString();
    }

    public void SetHealth(float health)
    {
        healthSlider.value = health;
    }
}
