﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject.SpaceFighter;

public class PauseController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Instances.PauseController = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PauseGame()
    {
        foreach(EnemyFacade enemy in Instances.EnemyRegistry.Enemies)
        {
            enemy.State = EnemyStates.Idle;
        }

        Time.timeScale = 0;
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        foreach (EnemyFacade enemy in Instances.EnemyRegistry.Enemies)
        {
            enemy.State = EnemyStates.Follow;
        }
    }
}
